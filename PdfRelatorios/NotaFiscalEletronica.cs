﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using PdfSharp.Pdf;

namespace PdfRelatorios
{
    public class NotaFiscalEletronica
    {
        public static PdfDocument GerarPdf(NotaFiscalDto dados)
        {
            var document = new Document();
            var sec = document.Sections.AddSection();
            sec.AddParagraph("ID:" + dados.NotaFiscalId);
            sec.AddParagraph("Numero:" + dados.Numero);
            sec.AddParagraph("Emissao:" + dados.DataEmissao);
            sec.AddParagraph("Prestador:" + dados.NomePrestador);
            return RenderDocument(document);
        }

        private static PdfDocument RenderDocument(Document document)
        {
            var rend = new PdfDocumentRenderer { Document = document };
            rend.RenderDocument();
            return rend.PdfDocument;
        }
    }
}
