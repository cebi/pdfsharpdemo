﻿using System;

namespace PdfRelatorios
{
    public class NotaFiscalDto
    {
        public int NotaFiscalId { get; set; }

        public int Numero { get; set; }

        public DateTime DataEmissao { get; set; }

        public string NomePrestador { get; set; }
    }
}
