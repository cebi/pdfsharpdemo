﻿using System.Web.Http;
using PdfApi.App_Start;

namespace PdfApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            GlobalConfiguration.Configuration.Formatters.Add(new PdfMediaTypeFormatter());
        }
    }
}
