﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using PdfApi.Models;
using PdfApi.Relatorios;
using PdfSharp.Pdf;

namespace PdfApi.Controllers
{
    public class TestController : ApiController
    {
        public HttpResponseMessage GetTest(string author)
        {
            var person = new Person { Name = author };

            // chamada para criacao de um relatorio especifico
            PdfDocument doc = PdfGenerator.CreatePdf(person.Name);

            return Request.CreateResponse(HttpStatusCode.OK, doc);
        }

    }
}