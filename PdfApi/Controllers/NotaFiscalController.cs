﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using PdfRelatorios;
using PdfSharp.Pdf;

namespace PdfApi.Controllers
{
    [RoutePrefix("NotaFiscal")]
    public class NotaFiscalController : ApiController
    {
        public HttpResponseMessage Get()
        {
            var notaFiscalDto = new NotaFiscalDto
            {
                NotaFiscalId = 1,
                Numero = 1234,
                DataEmissao = DateTime.Today,
                NomePrestador = "Cristiano Yamashita"
            };

            PdfDocument notaFiscal = NotaFiscalEletronica.GerarPdf(notaFiscalDto);

            return Request.CreateResponse(HttpStatusCode.OK, notaFiscal);
        }

        [HttpPost]
        [Route("Imprimir")]
        public IHttpActionResult Imprimir(NotaFiscalDto notaFiscalDto)
        {
            PdfDocument pdfDoc = NotaFiscalEletronica.GerarPdf(notaFiscalDto);

            byte[] fileContent;

            using (MemoryStream stream = new MemoryStream())
            {
                pdfDoc.Save(stream, true);
                fileContent = stream.ToArray();
            }

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(fileContent);
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline");
            
            //opens in tab, use 'attachment' to download instead
            response.Content.Headers.ContentDisposition.FileName = "NotaFiscal.pdf";
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

            return ResponseMessage(response);
        }
    }
}
